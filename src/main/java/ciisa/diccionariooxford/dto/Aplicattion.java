/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.diccionariooxford.dto;

import java.util.List;

/**
 *
 * @author andre
 */
public class Aplicattion {
    
    private String id;
    private Metadata metadata;
    private List<Results> results;
    private String word;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the metadata
     */
    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * @param metadata the metadata to set
     */
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    /**
     * @return the results
     */
    public List<Results> getResults() {
        return results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<Results> results) {
        this.results = results;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }
    
    
    
    
}
