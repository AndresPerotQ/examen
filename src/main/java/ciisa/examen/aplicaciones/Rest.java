/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.examen.aplicaciones;

import ciisa.diccionariooxford.dto.Aplicattion;
import ciisa.diccionariooxford.entity.Palabras;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author andre
 */

@Path("dicionario")
public class Rest {
        
    @GET
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response consultar(@PathParam("palabra") String palabra){
        
    
        Aplicattion palabraDTO = new Aplicattion();
        
        palabraDTO.setId(palabra);
        
        Client cliente=ClientBuilder.newClient();
        
        WebTarget recurso=cliente.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/"+palabra);
        
        Aplicattion palabra1= recurso.request(MediaType.APPLICATION_JSON).header("app_id","1ca5ed1b").header("app_key", "410523ca8eb3419270d02c80c9e1bd9e").get(Aplicattion.class);
        
        String definicion=palabra1.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
                
        return Response.ok(200).entity(palabraDTO).build();
                
        
    }
    
    
}
