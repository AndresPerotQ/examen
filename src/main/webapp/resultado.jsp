<%-- 
    Document   : resultado
    Created on : 23-10-2021, 16:40:10
    Author     : andre
--%>


<%@page import="ciisa.diccionariooxford.entity.Palabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% 
    Palabras palabra= (Palabras)request.getAttribute("palabra1");
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Palabra consultada: <%= palabra.getPalabra() %> </h1>
        <h1>Significado: <%= palabra.getSignificado() %></h1>
    </body>
</html>
