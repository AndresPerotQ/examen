/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.diccionariooxford.dto;

import java.util.List;

/**
 *
 * @author andre
 */
public class Senses {
    
    private List<String> definitions;
    private List<Examples> examples;
    private String id;

    /**
     * @return the definitions
     */
    public List<String> getDefinitions() {
        return definitions;
    }

    /**
     * @param definitions the definitions to set
     */
    public void setDefinitions(List<String> definitions) {
        this.definitions = definitions;
    }

    /**
     * @return the examples
     */
    public List<Examples> getExamples() {
        return examples;
    }

    /**
     * @param examples the examples to set
     */
    public void setExamples(List<Examples> examples) {
        this.examples = examples;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
    
    
}
