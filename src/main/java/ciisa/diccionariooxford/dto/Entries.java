/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciisa.diccionariooxford.dto;

import java.util.List;

/**
 *
 * @author andre
 */
public class Entries {
    
    private List<GrammaticalFeatures> grammaticalFeatures;
    private List<Senses> senses;

    /**
     * @return the grammaticalFeatures
     */
    public List<GrammaticalFeatures> getGrammaticalFeatures() {
        return grammaticalFeatures;
    }

    /**
     * @param grammaticalFeatures the grammaticalFeatures to set
     */
    public void setGrammaticalFeatures(List<GrammaticalFeatures> grammaticalFeatures) {
        this.grammaticalFeatures = grammaticalFeatures;
    }

    /**
     * @return the senses
     */
    public List<Senses> getSenses() {
        return senses;
    }

    /**
     * @param senses the senses to set
     */
    public void setSenses(List<Senses> senses) {
        this.senses = senses;
    }
    
    
}
